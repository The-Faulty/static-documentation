# [VTOLAPI](/documentation/api/index.html).CreateCommand

## Declaration
public void **CreateCommand**(string **command**, Action<string> **callBack**);

## Parameters
 command | The command prefix to activate the callBack 
-|-
**callBack** | **The method to call on receiving the command prefix in the console** 

## Description
Adds a command to the mod loader console that will call a function from your mod when entered.

```cs
public GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);

public override void ModLoaded()
{
    VTOLAPI.instance.CreateCommand("command", commandCallBack); //this is not a static method so you must create the command on the instance of the VTOLAPI
    base.ModLoaded();
}

private void commandCallBack(string command)
{
    if (command.Equals("command active")) //Make sure to include the command prefix here
    {
        cube.SetActive(true);
        cube.transform.position = new Vector3(0, 0, 0);
        Debug.Log("Cube set to active and moved");
    }
    else Debug.Log("Command called but unknown");
}
```