# [VTOLAPI](/documentation/api/index.html).SteamID

## Declaration
public static ulong **SteamID**();

## Returns
The player's Steam ID as an unsigned long.

## Description
SteamID returns the users steam id which can be used to identify the user's steam profile. This function uses Steamworks and returns as a ulong. 

```cs
Debug.Log("This is the users SteamID: " + VTOLAPI.SteamID())
```