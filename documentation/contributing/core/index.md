# Core

The goal of the Core library is to handle common tasks between the in-game code and launcher code.

Currently, there is only two main tasks the core library does.

# Enums

## ContentType

Currently, all content for VTOL VR Mods can be split up into 4 different types.

Mods, these are mods that have been release onto the website and downloaded to the `mods` folder in the games' directory.

Skins, these are skins that have been released onto the website and downloaded to the `skins` folder in the games' directory.

MyMods, these are mods that the user has created, stored in a septate projects folder set by the user. These folders are set up to work with `git` for source control.

MySkins, these are skins that the user has created, stored in a septate projects folder set by the user. These folders are just a basic folder with some `.png` files.

# Reading JSON files to C# Objects

The Mod Loader now makes big use of JSON files for storing data to disk and reading data. Every Mod/Skin has a `info.json` file which has important information about the item, so it can be displayed to the user.

Also, for dev tools, it makes use of storing what selections had been made in the launcher. This means when the mod creator launches the game up, it can load the `devtools.json` file and do actions such as automatically load up a scenario.

## BaseItem

BaseItem is the C# for every `info.json` file inside of mods/skins.

### Converter

BaseItem has a [custom converter](https://www.newtonsoft.com/json/help/html/CustomJsonConverter.htm). It makes sure that all JSON files can be upgraded to a new version.

#### ReadJson

At the top is a `readonly Dictionary<string, string>` which stores the old key names and what they correspond to now. 

For example, "Public ID" got changed to "public id" then to "pub_id". 


Let's say the converter is on key "Public ID"

On [line 56](https://gitlab.com/vtolvr-mods/ModLoader/-/blob/release/Core/Jsons/BaseItemConverter.cs#L56)
```cs
if (!_propertyMappings.TryGetValue(jp.Name, out string name))
```

It would check if "Public ID" **is not** in the propertyMappings. Because it is, it will skip this statement and return the updated value in `name`. 

So in the current version as of writing this, `name` would equal `pub_id`.

That is how it updates the JSON from old keys to new keys.

## DevTools

DevTools.cs is a class which holds information such as scenarios and mods. This for mod developers who need to boot up into a scenario without putting their VR headset on and going through all the menus.

It has just two methods for loading the JSON file into an object and saving the object into a JSON file.

## Scenario

Scenario.cs is a class used in the DevTools.cs file to store the scenarios as objects to hold information such as if they are a workshop scenario or a base game scenario. 

This is so later on, in-game, when loading them it can fetch and load them correctly.

It is mostly just a data class, but has two overrides to make life a little easier when using it.

# Logging

Both the in-game code and launcher code use different logging methods. So for the Core library to output log messages, the `Logger.cs` class was created.

It has a basic `Action` that other sections of code can subscribe events onto, to pass the log message on to the correct logger for that context.

Here is an example for inside of Unity.
```cs
private static void AttachCoreLogger()
{
    Core.Logger.OnMessageLogged += CoreLog;
}

private static void CoreLog(object message, Core.Logger.LogType type)
{
    switch (type)
    {
        case Core.Logger.LogType.Log:
            Debug.Log($"[Core] {message}");
            break;
        case Core.Logger.LogType.Warning:
            Debug.LogWarning($"[Core] {message}");
            break;
        case Core.Logger.LogType.Error:
            Debug.LogError($"[Core] {message}");
            break;
    }
}
```
[*ModLoader/ModLoaderManager.cs Line 424 - 443*](https://gitlab.com/vtolvr-mods/ModLoader/-/blob/release/ModLoader/ModLoaderManager.cs#L424)

Here we can see, it checks the type and passes it to the correct Unity log method.

# Steam Authentication

[`SteamAuthentication.cs`](https://gitlab.com/vtolvr-mods/ModLoader/-/blob/release/Core/SteamAuthentication.cs) is our very basic way in stopping people who pirate VTOL VR.

In our research into how differs between a normal VTOL VR game and a pirated version, it turns out they modify `steam_api64.dll`. This modification breaks the signature on the code from Valve.

This file, mostly copied from stackoverflow, uses `wintrust.dll` to see if the dll is still signed. 
```cs
public static bool IsTrusted(string fileName)
{
    return WinVerifyTrust(fileName) == 0;
}
```
[*SteamAuthentication.cs Line 43 - 46*](https://gitlab.com/vtolvr-mods/ModLoader/-/blob/release/Core/SteamAuthentication.cs#L43)

IsTrusted returns true if the file is signed, at that point we assume that this is a valid user who owns the game through Steam.