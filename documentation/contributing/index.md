# Contributing to the Mod Loader

>[!WARNING]
> This page is aimed at people who want to help with code or art.
>
> If you would like to support financially, please head over to our [support section](https://vtolvr-mods.com/#support) on the home page. Thank you ❤️

Firstly, thank you for considering contributing to the VTOL VR Mod Loader's code.

All the code that users download and install is [open source on GitLab](https://gitlab.com/vtolvr-mods/ModLoader). This C# solution is split up into six different projects.

## Builder

Framework: .NETFramework v4.5

Output Type: Console application

Language Version: C# 7.3

Builder is a simple application with the role of building and packaging files for release.

## Core

Framework: netstandard 2.0

Output Type: Class library

Language Version: C# 7.3

## Launcher

Framework: net5.0-windows

Output Type: Windows application

Language Version: c# 9.0

## ModLoader

Framework: .NETFramework v4.6.1

Output Type: Class library

Language Version: C# 7.3


## UnPatcher

Framework: .NETFramework v4.6.1

Output Type: Console application

Language Version: C# 7.3

## VTPatcher

Framework: .NETFramework v4.6.1

Output Type: Class library

Language Version: C# 7.3

# Contributing to the websites' code

The website is currently our only closed source project, however we are open to bringing others on board who want to help out with the site. Please contact a member of staff with why/what to help out.

The website's tech stack makes use of [Django](https://www.djangoproject.com/) for the majority of the site and [Vue.js](https://vuejs.org/) for the `/mods` and `/skins` list.