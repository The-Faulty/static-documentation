# AssetRipper

[![Github Image](https://img.shields.io/badge/Github-Repository-blue?style=for-the-badge&logo=github)](https://github.com/ds5678/AssetRipper) [![Latest Release of AssetRipper](https://img.shields.io/github/v/release/ds5678/AssetRipper?color=blue&label=Latest%20Release&logo=Github&style=for-the-badge)](https://github.com/ds5678/AssetRipper/releases/latest)

> AssetRipper is a tool for extracting assets from serialized files (CAB-*, *.assets, *.sharedAssets, etc.) and assets bundles (*.unity3d, *.bundle, etc.) and converting them into the native Unity engine format.

We can use AssetRipper's simple GUI application to convert VTOL VR back into a useable Unity Project. This page will cover the sets on how to do that.


## Preparing your game folder
Before you can begin converting game files using Asset Ripper, you need to prepare your game folder by performing the following steps:

1. Temporarily move the `VTOLVR_ModLoader` folder out of the game folder. This will prevent Asset Ripper from attempting to extract any files from this folder.

![Moved the VTOLVR_ModLoader folder out of the games files](/images/documentation/assetripper/moved-modloader.png)

2. Move any downloaded .mp3 files that you have inside the "RadioMusic" folder to a different location outside of the game's folder.
Here is what the default folder looks like. 

![Default Radio Music Folder](/images/documentation/assetripper/default-radiomusic.png)

3. Temporarily move "doorstop_config.ini" and "winhttp.dll" out of the game folder.


After completing these steps, you should verify your game files on Steam to ensure that you have a clean slate to work with.

![Verify files button](/images/documentation/assetripper/steam-verfiyfiles.png)

## Downloading AssetRipper

[![Latest Release of AssetRipper](https://img.shields.io/github/v/release/ds5678/AssetRipper?color=blue&label=Latest%20Release&logo=Github&style=for-the-badge)](https://github.com/ds5678/AssetRipper/releases/latest)

To use Asset Ripper, you first need to download and install the software by following these steps:

1. Download the latest release of Asset Ripper from the project's GitHub page.

![Download for windows 64](/images/documentation/assetripper/assetripper-download.png)

2. Extract the contents of the downloaded .zip file into a new folder on your computer.

![How it should look when installed](/images/documentation/assetripper/installed.png)

3. Launch AssetRipper.exe to start the program.

## Converting Game files

Once you have prepared your game folder and downloaded Asset Ripper, you can begin converting game files using the following steps:

1. Change the C# version to "C# 9" in Asset Ripper's settings.
2. Ensure that "Script Content Level" is set to "Level 2" in Asset Ripper's settings.

![Image of windows created by asset ripper](/images/documentation/assetripper/assetripper-windows.png)

3. Drag the entire "VTOL VR" folder into Asset Ripper.

![Dragging the VTOL VR Folder into AssetRipper](/images/documentation/assetripper/dragging-vtolfolder.gif)

4. Wait for AssetRipper to finish loading the game's assets, then export all files by selecting `Export > Export All Files` from the menu.

![Export button](/images/documentation/assetripper/assetripper-export.png)

5. Choose a new folder for the exported files to be saved in.
6. After the export has completed, delete the "AuxiliaryFiles" folder that Asset Ripper created.
7. Move the contents of the "ExportedProject" folder into the root folder you chose in step 5.
8. Delete the "LightingDataAsset" folder located in the "Assets" folder of the exported project.
9. Delete all the folders inside of "Assets > Scripts", except for `Assembly-CSharp`, `BDLearningBot`, and `VTBitConverter`.
10. Delete the `Assembly-CSharp-firstpass` folder located in the "Plugins" folder of the exported project.


## Opening and fixing the errors

AssetRipper displays the version of Unity that the game uses, along with a download link for that specific version. 

![Where the unity version is displayed](/images/documentation/assetripper/assetripper-unity-version.png)

Click on the link to open the download page, and then click the 'Install with Unity Hub' button to automatically download and install the correct version of Unity.

![Unity website with the mouse over the download link](/images/documentation/assetripper/unity-download-link.png)

>[!NOTE]
> The first time you open it will take the longest. Once you've opened it once, future times will be much quicker.

1. Open the exported project in Unity. If the "Enter in safe mode" prompt appears, select "Ignore".
2. In Unity's `Edit > Project Settings > Player` menu, change "Api Compatibility Level*" to ".NET 4.x" and "Active Input Handeling*" to "Both". Press "Apply" to save the changes and restart Unity.

![Changing project settings in unity](/images/documentation/assetripper/unity-project-settings.png)

3. After Unity has restarted, delete the "Hidden_UIElements_EditorUIE" shader located in the "Shader" folder of the exported project.
4. Open the Unity Package Manager. If the manager appears purple, close and reopen it.
5. Install `XP Plugin Management` and `TextMeshPro` using the Package Manager.
6. Copy the following DLLs from the "VTOLVR_Data > Managed" folder and paste them into the "Plugins" folder of the exported project:
    - Assembly-CSharp-firstpass.dll
    - CsvHelper.dll
    - Facepunch.Steamworks.Win64.dll
    - FlatBuffers.dll
    - HttpAuth.dll
    - K4os.Compression.LZ4.dll
    - MP3Sharp.dll
    - NAudio.dll
    - Oculus.LipSync.dll
    - Oculus.Platform.dll
    - Oculus.VR.dll
    - Rewired_Core.dll
    - Rewired_Windows.dll
    - SteamVR.dll
    - System.Buffers.dll
    - System.ComponentModel.Composition.dll
    - System.Memory.dll
    - System.Runtime.CompilerServices.Unsafe.dll
    - Unity.XR.Oculus.dll
    - Unity.XR.OpenVR.dll
    - Valve.Newtonsoft.Json.dll
7. Restart Unity
8. Got to "Edit > Project Settings > XR Plug-in Management" and check "Open XR"

![Selecting OpenXR in the project settings](/images/documentation/assetripper/unity-open-xr.png)

9. Clear the console and you should have a few manual code changes to do.


> [!WARNING]
> It is highly recommended that you make a back of this exported folders incase you change something and want to remember how it was like in the default game files.
